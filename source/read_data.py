import os
import glob
import cv2
import numpy as np
import matplotlib.pyplot as plt
#==============================================================================
batch_sz = 29
img_sz1 = 425
img_sz2 = 560
num_channels = 3 
"""
root_dir --> code
one dir up --> data==>images:: batches==> batches

"""
#==============================================================================
global root_dir 
root_dir =  os.path.normpath(os.getcwd() + os.sep + os.pardir)

#==============================================================================
def load_image(path):
    return cv2.imread(path,cv2.IMREAD_COLOR)

def imshow(data,title=''):
    plt.figure()
    plt.imshow(data[:,:,[2,1,0]])
    plt.title(title)
 
#==============================================================================
def get_file_list(path,ftype='\*png'):
    return glob.glob(path+ftype)

def join_path(root,new):
    return os.path.join(root,new)

def go_up_one_dir(cwd):
    return os.path.normpath(cwd + os.sep + os.pardir)
#==============================================================================


def main():
    data_path = join_path(root_dir,'data')
    data_path = join_path(data_path,'daquar')
    img_list = get_file_list(data_path)
     
    print('showing 10 random images')
    random_indexes = np.random.randint(0,len(img_list),size=10)
    for idx in random_indexes:
        imshow(load_image(img_list[idx]))
     
    num_images = len(img_list)
    num_batches = int(num_images/batch_sz)
     
    img_batch = np.zeros(shape=(img_sz1,img_sz2,num_channels,batch_sz),dtype=np.uint8)
    cwd = go_up_one_dir(data_path)
    os.mkdir(join_path(cwd,'batches'))
    cwd = join_path(cwd,'batches')
    file = join_path(cwd,'batch%s.npy')
    
    for batch_idx in range(num_batches):
        for img_idx in range(batch_sz):
            img = load_image(img_list[batch_idx*batch_sz+img_idx])
            img_batch[:,:,:,img_idx] = img
        np.save(file%(batch_idx),img_batch)
    
    #show sample image from one batch(batch 0 img 0)
    imshow(np.load(file%(0))[:,:,:,0])

if __name__ == '__main__':
    main()